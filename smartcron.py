#!/usr/bin/python3

# Super Mautic Cron
# Desenvolvedor: Daniel Cambria
# Data: 2019-04-10
# Última atualização: 2019-07-31
# Versão 1.4

# https://www.mautic.org/docs/en/setup/cron_jobs.html
# https://www.twentyzen.com/en/blog/faq-items/mautic-console-cronjobs/

# incluir na função def runMauticCommands(mautic, log) 
# onde log = True/False
# para evitar criação de log quando usado parâmetro -i

import subprocess, os
import datetime, time
import argparse, textwrap
from config import *
from appdir import *

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    #########################################################################################################

def runmautics():

    #inicia o loop infinito
    while True:
        
        #inicia o timer para mensurar o tempo de execução do loop
        runtime_loop_start = time.time()                    
        file = open(log, "a", encoding="UTF-8")        
        log_run_lineA = bcolors.FAIL + "\n====> ☕️  " + "INÍCIO DO LOOP, " + str(len(mautics)) + " MAUTICS RODANDO EM SÉRIE." + bcolors.ENDC
        file.write(log_run_lineA + "\n")
        file.close()            
        
        print(log_run_lineA) 
         
        #inicia o loop de instâncias
        for count_mautics, mautic in enumerate(mautics, start = 1):        
            
            #inicia o timer para mensurar o tempo de execução da instância mautic
            runtime_mautic_start = time.time()            
            print("") 
                    
            file = open(log, "a", encoding="UTF-8")    
            log_run_line0 = "LOG START AT: " + bcolors.BOLD + str(datetime.datetime.now()) + bcolors.ENDC 
            log_run_line1 = bcolors.OKGREEN + "====> ☕️  " + bcolors.BOLD + "INICIANDO " + bcolors.ENDC + bcolors.OKGREEN + "COMANDOS PARA " + bcolors.BOLD + mautic.upper() + " " + "(" + str(count_mautics) + "/" + str(len(mautics)) + ")" + bcolors.ENDC
            file.write("\n" + log_run_line0 + "\n")
            file.write(log_run_line1 + "\n")
            file.close()            
            
            print(log_run_line0)
            print(log_run_line1)
            
            #RODA COMANDOS DA INSTÂNCIA SELECIONADA
            runMauticCommands(mautic)
            
            #finaliza o timer da instância mautic
            runtime_mautic = round(time.time() - runtime_mautic_start, 2)            
            
            file = open(log, "a", encoding="UTF-8")        
            log_run_line5 = bcolors.OKGREEN + "====> ☕️  " + bcolors.BOLD + "ENCERRANDO " + bcolors.ENDC + bcolors.OKGREEN + "COMANDOS PARA " + bcolors.BOLD + mautic.upper() + " " + "(" + str(count_mautics) + "/" + str(len(mautics)) + ")" + bcolors.ENDC
            log_run_line6 = bcolors.OKGREEN + "====> ☕️  " + bcolors.OKGREEN + "TEMPO DE EXECUÇÃO: " + bcolors.ENDC + bcolors.BOLD + str(runtime_mautic) + " segundos" + bcolors.ENDC + " PAUSA: " + str(pause_between_mautics) + " SEGUNDOS" 
            log_run_line7 = bcolors.OKGREEN + "====> ☕️  " + bcolors.WARNING + "EXECUÇÃO FINALIZADA EM: " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + bcolors.ENDC
            file.write(log_run_line5 + "\n")
            file.write(log_run_line6 + "\n")
            file.write(log_run_line7 + "\n")
            file.close()            
            
            print(log_run_line5)
            print(log_run_line6)
            print(log_run_line7)
            
            time.sleep(pause_between_mautics)
                
        #finaliza o timer do loop
        runtime_loop = round(time.time() - runtime_loop_start, 2)            
        
        file = open(log, "a", encoding="UTF-8")        
        log_run_line7 = bcolors.FAIL + "\n====> ☕️  " + "TEMPO DE EXECUÇÃO DO LOOP: " +  str(runtime_loop) + " segundos" + bcolors.ENDC
        log_run_line8 = bcolors.FAIL + "====> ☕️  " + "PAUSA ANTES DO REINÍCIO: " + str(pausa_loop) + " SEGUNDOS"

        file.write(log_run_line7)
        file.write(log_run_line8 + "\n")
        file.close()            
        
        print(log_run_line7)
        print(log_run_line8)
        
        print ("")
        print ("")        
        print ("====> ☕️ HORA DO CAFÉ.")
        print ("")
        print ("                 ^  \\")
        print ("                (0)(0)")
        print ("                   >")
        print ("                ....")
        print ("")
        print ("                         descansando o servidor por " + str(pausa_loop) + " segundos...")
        print ("")
        
        time.sleep(pausa_loop)


def runMauticCommands(mautic):
    # executa os comandos na instancia atual
    for count_comandos, chave in enumerate(comandos, start = 1):
        
        #monta o comando conforme instância de mautic selecionada e a ordem de comandos no dicionário "comandos" 
        #executa 
        comando = precomando + ' php ' + mautics_path + mautic + '/' + 'app/console ' + comandos[chave][0]
        
        file = open(log, "a", encoding="UTF-8")
        log_run_line2 = bcolors.OKBLUE + "====> ☕️  " + "EXECUÇÃO: " + bcolors.ENDC + bcolors.HEADER + "(" + str(count_comandos) + "/" + str(len(comandos)) + ") " + " 🎸 " + mautic + bcolors.ENDC
        log_run_line3 = bcolors.OKBLUE + "====> ☕️  " + "COMANDO: " + bcolors.ENDC + bcolors.HEADER + comando + bcolors.ENDC      
        log_run_line3B = bcolors.OKBLUE + "====> ☕️  " + "FUNÇÃO: " + bcolors.ENDC + bcolors.HEADER + comandos[chave][1]  + bcolors.ENDC               
        file.write(log_run_line2 + "\n")
        file.write(log_run_line3 + "\n")
        file.write(log_run_line3B + "\n")
        file.close()            
        
        print (log_run_line2)
        print (log_run_line3)            
        print (log_run_line3B)            
        
        #inicia o timer para mensurar o tempo de execução do comando
        runtime_cmd_start = time.time()
        
        #executa o comando
        subprocess.call(comando, shell = True)
        
        #termina o timer para mensurar o tempo de execução do comando              
        runtime_cmd = time.time() - runtime_cmd_start
                    
        file = open(log, "a", encoding="UTF-8")                        
        log_run_line4 = bcolors.OKBLUE + "====> ☕️  " + "EXECUÇÃO: " + bcolors.ENDC + bcolors.HEADER + "(" + str(count_comandos) + "/" + str(len(comandos)) + ") " + " 🎸 " + mautic + bcolors.ENDC + bcolors.WARNING + " CONCLUÍDO" + bcolors.ENDC         
        log_run_line5 = bcolors.UNDERLINE + "====> ☕️  " + "TEMPO DE EXECUÇÃO: %s segundos ---" % round(runtime_cmd, 2) + " PAUSA: " + str(pause_between_commands) + " SEGUNDOS" + bcolors.ENDC         
        log_run_line6 = bcolors.OKGREEN + "====> ☕️  " + bcolors.WARNING + "EXECUÇÃO FINALIZADA EM: " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + bcolors.ENDC        
        file.write(log_run_line4 + "\n")
        file.write(log_run_line5 + "\n")
        file.write(log_run_line6 + "\n")
        file.close()            
        
        print(log_run_line4)            
        print(log_run_line5)
        print(log_run_line6)
        
        time.sleep(pause_between_commands)        

def console(cmd):
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    out, err = p.communicate()
    return (p.returncode, out, err)

def version_check():
    # Realiza a verificação de versão
    git_fetch = 'git -C ' + smartcron_dir + ' fetch origin'
    git_newlog = 'git -C ' + smartcron_dir + ' log HEAD..origin/master --oneline'
    
    git_fetch=console(git_fetch)[1]
    git_newlog=console(git_newlog)[1]
    
    if git_fetch == git_newlog:
        print("")
        print(bcolors.OKBLUE + '====> ☕️ Up-to-date' + bcolors.ENDC)
        print("")
    else:
        print("")        
        print(bcolors.OKGREEN + "====> ☕️ NOVA VERSÃO! Atualize agora com smartcron --update" + bcolors.ENDC)
        print("")        

# Monta caminho do arquivo de log. Não altere.
log = log_path + log_file

# Inicia o parser
parser = argparse.ArgumentParser(
    prog='mauticsmartcron',
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description=textwrap.dedent(bcolors.BOLD + '''SMARTCRON
    O Cron Inteligente para Múltiplas Instâncias de Mautic.''' + bcolors.ENDC),
    usage='%(prog)s [options]',
    epilog="Desenvolvido por Daniel Cambría daniel.cambria@derosemethod.org.\nBureau de Tecnologia, 2019 - Todos os direitos reservados.",
)
# Configuração das opções do parser
parser.add_argument("-i", default=None, nargs='?', help="Executa comandos apenas para uma instância")
parser.add_argument("--update", help="Atualiza a aplicação", action="store_true")
parser.add_argument("--loop", help="Ativa loop infinito", action="store_true")
args = parser.parse_args()

# Opções do parser para --loop
if args.loop == True:
    runmautics()    

# Opções do parser para -i
if args.i != None:
    #captura a instância de Mautic
    mautic = args.i
    #roda comendos
    runMauticCommands(mautic)

# Parser para atualizar aplicação
elif args.update == True:
    comando_gitpull="git -C " + smartcron_dir + " pull"
    comando_kill="pkill smartcron"
    
    print(bcolors.OKGREEN + "====> ☕️ Fazendo update..." + bcolors.ENDC)   
    subprocess.call(comando_gitpull, shell = True)    
    print("")
    print(bcolors.OKGREEN + "====> ☕️ SMARTCRON atualizado!" + bcolors.ENDC)    
    print(bcolors.WARNING + "====> ☕️ Matando processo iniciado com smartcron-bg em background..." + bcolors.ENDC)    
    print(bcolors.BOLD + "====> ☕️ Se precisar, reinicie o smartcron-bg" + bcolors.ENDC)    
    subprocess.call(comando_kill, shell = True)    
    
else:    
    parser.print_help()
    version_check()