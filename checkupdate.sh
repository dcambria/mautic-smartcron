#!/bin/sh
UPSTREAM=
LOCAL=$(git -C $SMARTCRON_DIR rev-parse @)
BASE=$(git -C $SMARTCRON_DIR merge-base @ "$UPSTREAM")

if [ $LOCAL = $REMOTE ]; then
    echo "Up-to-date"
elif [ $LOCAL = $BASE ]; then
    echo "Nova versão! Atualize com smartcron -U"
fi
