# https://stackoverflow.com/questions/10408816/how-do-i-use-the-nohup-command-without-getting-nohup-out
# https://askubuntu.com/questions/57477/bring-nohup-job-to-foreground
echo "Iniciando Smartcron em background!"
echo "Para matar o processo, use: "
echo "ps -eo pid,comm,lstart,etime,time,args |grep mautic"
echo "kill -9 [numero do processo]"
nohup smartcron --loop </dev/null >/dev/null 2>&1 &

#Mostra processo em background:
# ps -eo pid,comm,lstart,etime,time,args |grep mautic

#Mata processo:
#	kill PID
