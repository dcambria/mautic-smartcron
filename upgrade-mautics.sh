#!/bin/bash
# 
# Mautics Upgrader
# ----------------
# Aplica atualizações em série para todos os mautics instalados na máquina.
# Defina o diretório raiz das instalações de Mautic e rode o script. 
#
# Desenvolvido por Daniel Cambría
# Versão 0.1
# Criado em: 2019-08-04
# Última revisão: 2019-08-14

mautics_dir=/var/www/

cd $mautics_dir

#armazena todos os mautics em array
mautics=($(ls -d mautic*))

#conta as instâncias de mautic
#mautics_total=($(ls -d mautic* |wc -w))

#loop do array mautics
for i in ${mautics[*]}; do echo && echo Upgrading $i: && sudo php $i/upgrade.php; done

echo Upgrades finalizados.
