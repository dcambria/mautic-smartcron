# antigos comandos.
# não vi necessidade de utilizá-los
# e também são demorados.

#find . -type d -exec sudo chmod 755 {} \;
#find . -type f -exec sudo chmod 644 {} \;
#sudo chmod -R g+w themes/
#sudo chown -R $USER:www-data .

#/var/www/mautic-onlineschool/app/config/local.php
#/var/www/mautic-onlineschool/app/cache
#/var/www/mautic-onlineschool/app/logs
#/var/www/mautic-onlineschool/media/files
#/var/www/mautic-onlineschool/media/images
#/var/www/mautic-onlineschool/translations
#/var/www/mautic-onlineschool/app/spool

# Seta cores
RED='\033[0;31m'
HEADER='\033[95m'
OKBLUE='\033[94m'
OKGREEN='\033[92m'
WARNING='\033[93m'
FAIL='\033[91m'
BOLD='\033[1m'
UNDERLINE='\033[4m'
ENDC='\033[0m'


# Define o diretório padrão do Mautic.
DIR="/var/www/"

clear
echo -e "${HEADER}Diretório de instação do Mautic configurado:${ENDC}"
echo ""
echo -e "${OKBLUE}=> $DIR${ENDC}"
echo ""
echo -e "${OKGREEN}Listando instâncias de Mautic:"
echo ""
ls -ld /var/www/mautic*
echo -e "${ENDC}"
echo -e "${WARNING}Pressione [Enter] para corrigir as permissões em $DIR"
echo -e -n "ou digite um novo diretório (copie o caminho da lista acima): ${ENDC}"
read NEWDIR

# Entra no diretório selecionado ou manté o diretório padrão.
if [[ $NEWDIR = "" ]]; then
    NEWDIR=$DIR
fi

# Rotina para apagar cache
echo ""
echo ""
echo -e "${HEADER}----------------"
echo "DELEÇÃO DE CACHE"
echo -e "----------------${ENDC}"
echo ""
echo -e -n "${WARNING}Deletar cache em $NEWDIR? [s/N] ${ENDC}"
read CONTINUA

if [[ $CONTINUA != "s" ]]; then
    echo "Cache mantido inalterado."
    else 
        # Apaga cache
        echo "Apagando Cache em $NEWDIR/app/cache/"
        sudo rm -rf $NEWDIR/app/cache/*
        echo -e "${OKGREEN}Cache deletado.${ENDC}"
fi

# Rotina para corrigir permissões
echo ""
echo ""
echo -e "${HEADER}----------------------"
echo "CORREÇÃO DE PERMISSÕES"
echo -e "----------------------${ENDC}"
echo ""
echo -e -n "${WARNING}Corrigir permissões em $NEWDIR? [s/N] ${ENDC}"
read CONTINUA

if [[ $CONTINUA != "s" ]]; then
    echo "Permissões mantidas inalteradas."
    else
        # Realiza as correções no novo diretório.
        echo "Corrigindo Permissões em $NEWDIR"
        sudo chmod -R 775 $NEWDIR
        sudo chown -R www-data:www-data $NEWDIR
        sudo chmod -R 777 $NEWDIR/app/cache/    
        echo ""
        ls -ld /var/www/mautic*
        echo ""
        echo -e "${OKGREEN}Correções realizadas.${ENDC}"
        echo ""
fi


# Rotina para corrigir permissões
echo ""
echo ""
echo -e "${HEADER}----------------------"
echo "Limpeza de Spool"
echo -e "----------------------${ENDC}"
echo ""
echo -e -n "${WARNING} Eliminar todas as mensagens em $NEWDIR/app/spool/default/ ? [s/N] ${ENDC}"
read CONTINUA

if [[ $CONTINUA != "s" ]]; then
    echo "Spool inalterado."
    else
        # Realiza as correções no novo diretório.
        echo "Eliminando spool em $NEWDIR/app/spool/default/"
        for MESSAGE in $NEWDIR/app/spool/default/./* ; do sudo rm "$MESSAGE";done
        echo ""
        echo -e "${OKGREEN}Spool apagado.${ENDC}"
        echo ""
fi

# Rotina para atualizar GeoIP
echo ""
echo ""
echo -e "${HEADER}----------------"
echo "Atualiza GeoIP"
echo -e "----------------${ENDC}"
echo ""
echo -e -n "${WARNING}Atualizar GeoIP em $NEWDIR? [s/N] ${ENDC}"
read CONTINUA

if [[ $CONTINUA != "s" ]]; then
    echo "GeoIP não atualizado"
    else
        # Apaga cache
        echo "Atualizando GeoIP em $NEWDIR/app/cache/"
	sudo -u www-data mkdir -p $NEWDIR/app/cache/ip_data
	sudo -u www-data geoipupdate -f /etc/GeoIP.conf -d $NEWDIR/app/cache/ip_data -v
        echo -e "${OKGREEN}GeoIP atualizado.${ENDC}"
fi
