# CONFIGURAÇÕES MAUTIC SMART-CRON
# Atualize os dados abaixo para alterar o comportamento desse sistema.


# Caminhos do sistema
mautics_path = "/var/www/"
log_path = "./mautic-smartcron/"
log_file = "mautics-run.log"    

# Definição de pré-comando
# sudo -u permite que usuário root chame outro usuário para a
# executa comando usando usuário www-data
precomando = 'sudo -u www-data'

# Instancias para incluir no sistema.
# Os nomes da lista abaico devem ser exatamente igual ao diretório de instalação
# Altere os nomes abaixo.
# Comente e descomente mantendo as instalações entre "" e sempre com uma vírgula , no fim.
mautics = [
           "mautic-1",
           "mautic-2",
           "mautic-3",
           "mautic-4",
           #"mautic-5",
           #"mautic-6",
           #"mautic-7",
           #"mautic-8",
           ]

# Tempos ociosos do script. 
# Necessário para não sobrecarregar o servidor.
pause_between_commands = 5
pause_between_mautics = 60
pausa_loop = 240

# INSIRA OS NOVOS COMANDOS ABAIXO, adicionando entradas no dicionário
# Processar comando S/N, Comando, Descrição do comando
# comando: php mautics_path + mautics[instancia] + comandos[comando][1]
# Referência: https://www.twentyzen.com/en/blog/faq-items/mautic-console-cronjobs/
# Ver lista completa no comando php ./app/aconsole

comandos = {
    1:  ["mautic:segments:update --force --batch-limit=300", "Update contacts in smart segments based on new contact data. Always required! (should be scheduled first)"],
    2:  ["mautic:campaigns:rebuild --force --batch-limit=300", "Rebuild campaigns based on contact segments. Always required! (should be scheduled second)"],
    3:  ["mautic:campaigns:trigger --force --batch-limit=100", "Trigger timed events for published campaigns. Always required! (should be scheduled third)"],
#    4:  ["mautic:assets:generate", "Combines and minifies asset files from each bundle into single production files"],
    5:  ["mautic:broadcasts:send", "Process contacts pending to receive a channel broadcast."],
#    6:  ["mautic:campaigns:execute", "Execute specific scheduled events."],
#    7:  ["mautic:campaigns:messagequeue", "Process sending of messages queue."],
#    8:  ["mautic:campaigns:messages", "Process sending of messages queue."],
#    9:  ["mautic:campaigns:update", "Rebuild campaigns based on contact segments."],
#    10: ["mautic:campaigns:validate", "Validate if a contact has been inactive for a decision and execute events if so."],
#    11: ["mautic:citrix:sync", "Synchronizes registrant information from Citrix products"],
#    12: ["mautic:contacts:deduplicate", "Merge contacts based on same unique identifiers"],    
    13: ["mautic:email:fetch", "Fetch and process monitored email."],
#    14: ["mautic:emails:fetch", "Fetch and process monitored email."],    
    15: ["mautic:emails:send", "Processes SwiftMail's mail queue"],
    16: ["mautic:import", "Imports data to Mautic"],   
#    17: ["mautic:install:data", "Installs Mautic with sample data"],
#    18: ["mautic:integration:fetchleads", "Fetch leads from integration."],
#    19: ["mautic:integration:pipedrive:fetch", "Pulls the data from Pipedrive and sends it to Mautic"],
#    20: ["mautic:integration:pipedrive:push", "Pushes the data from Mautic to Pipedrive"],
#    21: ["mautic:integration:pushactivity", "Push lead activity to integration."],
#    22: ["mautic:integration:pushleadactivity", "Push lead activity to integration."],
#    23: ["mautic:integration:synccontacts", "Fetch leads from integration."],
    24: ["mautic:iplookup:download", "Fetch remote datastores for IP lookup services that leverage local lookups"],
#    25: ["mautic:maintenance:cleanup --days-old=90 --no-interaction", "Updates the Mautic application"],
#    26: ["mautic:messages:send", "Process sending of messages queue."],
#    27: ["mautic:migrations:generate", "Generate a blank migration class."],
#    28: ["mautic:plugins:install", "Installs, updates, enable and/or disable plugins."],
#    29: ["mautic:plugins:reload", "Installs, updates, enable and/or disable plugins."],
#    30: ["mautic:plugins:update", "Installs, updates, enable and/or disable plugins."],
#    31: ["mautic:queue:process", "Process queues"],
#    32: ["mautic:reports:scheduler", "Processes scheduler for report's export"],
#    33: ["mautic:segments:check-builders", "Compare output of query builders for given segments"],
#    34: ["mautic:segments:rebuild", "Update contacts in smart segments based on new contact data."],
    35: ["mautic:social:monitoring", "Looks at the records of monitors and iterates through them."],  
#    36: ["mautic:theme:json-config", "Converts theme config to JSON from PHP"],
#    37: ["mautic:transifex:pull", "Fetches translations for Mautic from Transifex"],
#    38: ["mautic:transifex:push", "Pushes Mautic translation resources to Transifex"],
#    39: ["mautic:translation:createconfig", "Create config.php files for translations"],
#    40: ["mautic:translation:debug", "Displays translation messages informations"],
#    41: ["mautic:update:apply", "Updates the Mautic application"],
#    42: ["mautic:update:find", "Fetches updates for Mautic"],
#    43: ["mautic:webhooks:process", "Process queued webhook payloads"],
}
