# Mautic SmartCron

Criado para enfileirar os cronjobs do **[Mautic](https://www.mautic.org)** de diversas instâncias num mesmo servidor **Ubuntu**, com a finalidade de minimizar a carga na máquina. Com isso, substitui o uso do **crontab**, uma vez que os comandos são executados um a um, sequencialmente.



## Configuração

* Altere config.py com suas preferências
* Rode o script em foreground com "mauticsmartcron.py" (quebre com crtl+c)
* Rode o script em background com "mauticsmartcron-bg.sh" (monitore o log com logview.sh)
* Corrija permissões e cache com "fixmautic.sh"


# Créditos
Desenvolvido por Daniel Cambría em [Bureau de Tecnologia](bureau-it.com).


# Licença
Licenciado para uso sob condições do GNU GPLv3.
