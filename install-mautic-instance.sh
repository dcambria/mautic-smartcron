# Instala instância de Mautic

# Seta cores
RED='\033[0;31m'
HEADER='\033[95m'
OKBLUE='\033[94m'
OKGREEN='\033[92m'
WARNING='\033[93m'
FAIL='\033[91m'
BOLD='\033[1m'
UNDERLINE='\033[4m'
ENDC='\033[0m'

# Diretório raiz para instalação
MAUTIC_DIR="/var/www/"

echo -e -n "${WARNING}Digite o diretório para instação do Mautic (geralmente mautic-nome) [$MAUTIC_DIR] : ${ENDC}"
read INSTALL

echo "Certifique-se de que o apache e php estão configurados."
cd $MAUTIC_DIR
sudo mkdir $INSTALL
sudo wget https://www.mautic.org/download/latest
sudo unzip latest -d $INSTALL/

echo "Baixando o Plugin MauticRssToEmailBundle"
echo "Instale pelo dashboard de configurações de Plugins do Mautic"
sudo -u www-data git clone https://github.com/ChrisRAoW/mautic-rss-to-email-bundle.git plugins/MauticRssToEmailBundle

echo "Corrigindo permissões e instalando GeoIP"
echo "Digite 's' para todas as opções"
fixmautic

echo "Removendo latest zip..."
sudo rm latest

echo "Mautic instalado."
