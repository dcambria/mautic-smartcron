# Seta cores
RED='\033[0;31m'
HEADER='\033[95m'
OKBLUE='\033[94m'
OKGREEN='\033[92m'
WARNING='\033[93m'
FAIL='\033[91m'
BOLD='\033[1m'
UNDERLINE='\033[4m'
ENDC='\033[0m'

clear
echo -e "${BOLD}INSTALADOR SMARTCRON${ENDC}"
echo ""

# Seta o diretório de instalação da aplicação e armazena em arquivo como variável python e monta script.
DIR=$(pwd)
printf smartcron_dir='"'$DIR'"' > appdir.py

# Cria executável logview.sh e seta permissão de execução
printf "tail -f $DIR/run.log" > logview.sh
chmod +x $DIR/logview.sh

echo -e "${OKBLUE}Arquivos de configuração instalados com sucesso.${ENDC}"
echo ""

# Verifica se os arquivos estão no diretório antes de iniciar.
if [ -f "$DIR/fixmautic.sh" ]

    # Inicia instalador
    then
        echo -e -n "${WARNING}[I]nstalar ou [R]emover o SmartCron em /usr/local/bin ? [i/r/Cancelar] ${ENDC}"
        read INSTALL
        
        if [[ $INSTALL = "i" ]]; then
            sudo ln -s $DIR/fixmautic.sh /usr/local/bin/fixmautic
            sudo ln -s $DIR/smartcron.py /usr/local/bin/smartcron
            sudo ln -s $DIR/smartcron-bg.sh /usr/local/bin/smartcron-bg
            sudo ln -s $DIR/logview.sh /usr/local/bin/smartcron-logview
            sudo ln -s $DIR/install-mautic-instance.sh /usr/local/bin/installmautic
            echo -e "${OKGREEN}Comandos instalados com sucesso.${ENDC}"
            echo -e "${OKGREEN}Altere o config-sample.py para config.py e ajuste o arquivo com as suas preferência.${ENDC}"

        elif [[ $INSTALL = "r" ]]; then
            sudo rm /usr/local/bin/fixmautic
            sudo rm /usr/local/bin/smartcron
            sudo rm /usr/local/bin/smartcron-bg
            sudo rm /usr/local/bin/smartcron-logview
            sudo rm /usr/local/bin/installmautic
            echo -e "${OKGREEN}Comandos removidos com sucesso.${ENDC}"
        else
            echo -e "${FAIL}Saindo...${ENDC}"
        fi

    # Sai do instalador se os arquivos não forem encontrados.
    else
        echo "Arquivos não encontrados. Tente rodar o instalador dentro do diretório do programa."
        echo -e "${FAIL}Saindo...${ENDC}"
fi
